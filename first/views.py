import datetime

from django.contrib.auth.decorators import login_required
from django.shortcuts import render

from first.forms import CalcForm
from first.models import CalcHistory


def get_base_context(pagename):
    """
    Получение базового контекста

    :param request: объект c деталями запроса.
                    Используется для получения авторизованного пользователя
    :type request: :class:`django.http.HttpRequest`
    :return: словарь с предустановленными значениями
    :rtype: :class:`dict`
    """
    menu = [
        {'link': '/calc', 'text': 'Калькулятор'},
    ]

    return {
        'sitename': 'Demosite',
        'pagename': pagename,
        'menu': menu,
    }


def main_page(request):
    """
    Заглавная страница

    :param request: объект c деталями запроса
    :type request: :class:`django.http.HttpRequest`
    :return: объект ответа сервера с HTML-кодом внутри
    :rtype: :class:`django.http.HttpResponse`
    """
    context = get_base_context('Добро пожаловать!')
    creation_date = datetime.datetime.now()
    context['pages'] = 3
    context['auth'] = 'Andrew'
    context['cr_date'] = creation_date
    context['user'] = request.user
    return render(request, 'index.html', context)


@login_required
def calc(request):
    context = get_base_context("Калькулятор")

    user = request.user

    all_data = CalcHistory.objects.filter(author=user)
    context['history'] = all_data

    if request.method == 'POST':  # Обрабатываем введённые данные
        calcform = CalcForm(request.POST)
        context['data_sent'] = True
        if calcform.is_valid():  # данные правильные, работаем с ними
            value_a = calcform.data['first']
            value_b = calcform.data['second']
            value_c = int(value_a) + int(value_b)
            record = CalcHistory(date=datetime.datetime.now(),
                                 first=value_a, second=value_b,
                                 result=value_c, author=user)
            record.save()
            context.update({
                'first_value': value_a,
                'second_value': value_b,
                'result': value_c,
                'form': calcform,
                'history': all_data
            })
        else:
            context['form'] = calcform
    else:
        calcform = CalcForm()
        context.update({
            'history': all_data,
            'form': calcform
        })

    return render(request, 'calc.html', context)
