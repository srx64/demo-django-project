from django.contrib.auth.models import User
from django.test import TestCase, Client


from django.urls import reverse


class IndexTest(TestCase):

    def setUp(self):
        client = Client()
        self.response = client.get(reverse('index'))

    def test_index_response(self):
        self.assertEqual(self.response.status_code, 200)

    def test_index_context(self):
        self.assertEqual(self.response.context['pages'], 3)
        self.assertEqual(self.response.context['auth'], 'Andrew')
        self.assertTrue('cr_date' in self.response.context)


class CalculatorTest(TestCase):
    fixtures = ['test_database.json', ]

    def setUp(self):
        self.client = Client()
        user = User.objects.get(username='vasya')
        self.client.force_login(user)

    def test_auth_redirect(self):
        client = Client()
        response = client.get(reverse('calc'), follow=True)
        self.assertEqual(response.status_code, 200)
        url = response.redirect_chain[-1][0]
        self.assertIn(reverse('login'), url)

    def test_simple_get(self):
        response = self.client.get(reverse('calc'))
        self.assertEqual(response.status_code, 200)

    def test_simple_post(self):
        response = self.client.post(reverse('calc'), {'first': '10', 'second': '20'})
        self.assertEqual(response.context['result'], 30)

    def test_invalid_post(self):
        response = self.client.post(reverse('calc'), {'first': '10'})
        self.assertFormError(response, 'form', 'second', ['This field is required.'])